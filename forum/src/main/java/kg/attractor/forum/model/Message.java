package kg.attractor.forum.model;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private Integer topicId;

    @Column(length = 128)
    private Integer userId;

    @Column(length = 128)
    private String content;

    @Column(length = 128)
    private String login;

    @Column
    private LocalDate messageDate;

    @Column
    private LocalTime messageTime;

}
