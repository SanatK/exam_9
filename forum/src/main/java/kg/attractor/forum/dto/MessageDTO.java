package kg.attractor.forum.dto;

import kg.attractor.forum.model.Message;
import kg.attractor.forum.model.Topic;
import lombok.*;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MessageDTO {

    private Integer id;
    private String login;
    private String content;
    private LocalDate date;
    private LocalTime time;

    public static MessageDTO from(Message message){
        return builder()
                .id(message.getId())
                .date(message.getMessageDate())
                .login(message.getLogin())
                .content(message.getContent())
                .time(message.getMessageTime())
                .build();
    }


}
