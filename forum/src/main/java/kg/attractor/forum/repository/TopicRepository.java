package kg.attractor.forum.repository;

import kg.attractor.forum.model.Topic;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TopicRepository extends JpaRepository<Topic, Integer> {
    List<Topic> findAll();
    Topic findTopicById(Integer id);
}
