package kg.attractor.forum.dto;

import kg.attractor.forum.model.Topic;
import lombok.*;

import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TopicDTO {

    private Integer id;
    private LocalDate date;
    private String name;
    private String login;
    private String question;
    private int quantity;
    private String theme;

    public static TopicDTO from(Topic topic){
        return builder()
                .id(topic.getId())
                .name(topic.getName())
                .date(topic.getTopicDate())
                .login(topic.getLogin())
                .theme(topic.getTheme())
                .question(topic.getQuestion())
                .quantity(topic.getQuantity())
                .build();
    }
}
