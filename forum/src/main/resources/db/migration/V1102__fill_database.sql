use `forum`;


INSERT INTO `topics` VALUES (1,1,'Real madrid','Football','Lets talk about Real Madrid','sana',0,'2020-05-23','10:51:31'),
(2,1,'Java programming','IT','How to fix NullPointerException','sana',11,'2020-05-23','10:52:12'),
(3,1,'New Star Wars','Movie ','What to you think about new star wars?','sana',0,'2020-05-23','10:53:52'),
(4,1,'Mercedes or BMW','Cars','Which do you prefer?','sana',0,'2020-05-23','10:54:26'),
(5,1,'How to fix','Help','Please help fix bug','sana',0,'2020-05-23','10:56:05'),
(6,1,'Interesting places','Places','Bishkek interesting places','sana',0,'2020-05-23','10:57:10'),
(7,1,'Spring','Java','Cant add annotations','sana',0,'2020-05-23','10:57:52'),
(8,1,'Forums','IT','Are Stack Exchange sites forums?','sana',0,'2020-05-23','10:59:26'),
(9,1,'Testing pagination','Page','Test Pageable','sana',0,'2020-05-23','11:00:03'),
(10,1,'Apple','IT','What do you think about this company','sana',0,'2020-05-23','11:00:55'),
(11,1,'Best prices','Life','Talk about shops with best prices','sana',0,'2020-05-23','11:01:39'),
(12,1,'Car or Bicycle','Transport','What do you like more< car or bicycle','sana',0,'2020-05-23','11:02:30'),
(13,1,'Gym','Health','Are you work out in the gym?\r\n','sana',0,'2020-05-23','11:03:35');

INSERT INTO `messages` VALUES
(1,2,1,'This is my favorite sport club!','sana','2020-05-23','10:52:51'),
(2,2,1,'When you declare a reference variable (i.e. an object) you are really creating a pointer to an object. Consider the following code where you declare a variable of primitive type int','sana','2020-05-23','11:04:20'),
(3,2,1,'Do you try to debug it?','sana','2020-05-23','11:04:47'),
(4,2,1,'In which case, you are not creating the object obj, but rather assuming that it was created before the doSomething() method was called. Note, it is possible to call the method like this:','sana','2020-05-23','11:05:06'),
(5,2,1,'NullPointerExceptions are exceptions that occur when you try to use a reference that points to no location in memory (null) ','sana','2020-05-23','11:05:21'),
(6,2,1,'I dont like it','sana','2020-05-23','11:07:53'),
(7,2,1,'This is my favorite sport club!','sana','2020-05-23','10:52:51'),
(8,2,1,'When you declare a reference variable (i.e. an object) you are really creating a pointer to an object. Consider the following code where you declare a variable of primitive type int','sana','2020-05-23','11:04:20'),
(9,2,1,'You need to try restart program','sana','2020-05-23','11:04:47'),
(10,2,1,'NullPointerExceptions are exceptions that occur when you try to use a reference that points to no location in memory (null) ','sana','2020-05-23','11:05:21'),
(11,2,1,'I dont like it','sana','2020-05-23','11:07:53');

