package kg.attractor.forum.service;

import kg.attractor.forum.dto.TopicDTO;
import kg.attractor.forum.model.Topic;
import kg.attractor.forum.model.User;
import kg.attractor.forum.repository.TopicRepository;
import kg.attractor.forum.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;



@AllArgsConstructor
@Service
public class TopicService {
    private final UserRepository userRepository;
    private final TopicRepository topicRepository;



    public void createNewTopic(String userEmail, String topicName, String topicTheme, String question){
        User user = userRepository.findUserByEmail(userEmail);
        var topic = Topic.builder()
                .name(topicName)
                .theme(topicTheme)
                .question(question)
                .userId(user.getId())
                .quantity(0)
                .login(user.getLogin())
                .topicDate(LocalDate.now())
                .topicTime(LocalTime.now())
                .build();

        topicRepository.save(topic);
    }

    public Page<TopicDTO> getAll(Pageable pageable){
        return topicRepository.findAll(pageable)
                .map(TopicDTO::from);
    }
}
