package kg.attractor.forum.service;

import kg.attractor.forum.exception.UserAlreadyExistException;
import kg.attractor.forum.exception.UserNotFoundException;
import kg.attractor.forum.model.User;
import kg.attractor.forum.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;


    public User getByEmail(String email){
        var user = userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);

        return user;
    }

    public User register(User user){

        if(userRepository.existsByEmail(user.getEmail())){
            throw new UserAlreadyExistException();
        }
        var newUser = User.builder()
                .name(user.getName())
                .email(user.getEmail())
                .login(user.getLogin())
                .password(passwordEncoder.encode(user.getPassword()))
                .build();

        userRepository.save(newUser);

        return newUser;
    }
}
