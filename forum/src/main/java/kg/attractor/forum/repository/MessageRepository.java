package kg.attractor.forum.repository;
import kg.attractor.forum.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface MessageRepository extends JpaRepository<Message, Integer> {
    Page<Message> findAllByTopicId(Integer topicId, Pageable pageable);
}
