package kg.attractor.forum.model;
import lombok.*;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@Table(name = "topics")
public class Topic {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(length = 128)
    private Integer userId;

    @Column(length = 128)
    private String name;

    @Column(length = 128)
    private String theme;

    @Column(length = 128)
    private String question;

    @Column(length = 128)
    private String login;

    @Column
    private Integer quantity;

    @Column
    private LocalDate topicDate;

    @Column
    private LocalTime topicTime;

}
