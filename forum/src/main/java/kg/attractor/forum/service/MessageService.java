package kg.attractor.forum.service;

import kg.attractor.forum.dto.MessageDTO;
import kg.attractor.forum.model.Message;
import kg.attractor.forum.model.Topic;
import kg.attractor.forum.model.User;
import kg.attractor.forum.repository.MessageRepository;
import kg.attractor.forum.repository.TopicRepository;
import kg.attractor.forum.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.time.LocalDate;
import java.time.LocalTime;


@Service
@AllArgsConstructor
public class MessageService {
    private final MessageRepository messageRepository;
    private final UserRepository userRepository;
    private final TopicRepository topicRepository;


    public Page<MessageDTO> getThisTopicMessages(int topicId, Pageable pageable){
        return messageRepository.findAllByTopicId(topicId, pageable)
                .map(MessageDTO::from);
    }

    public String createNewMessage(Integer topicId, String content, String userEmail){
        User user = userRepository.findUserByEmail(userEmail);
        messageRepository.save(Message.builder()
                                .topicId(topicId)
                                .content(content)
                                .userId(user.getId())
                                .login(user.getLogin())
                                .messageDate(LocalDate.now())
                                .messageTime(LocalTime.now())
                                .build());
        int newQuantity = topicRepository.findTopicById(topicId).getQuantity()+1;
        Topic topic = topicRepository.findTopicById(topicId);
        topic.setQuantity(newQuantity);
        topicRepository.save(topic);
        return "success";
    }
}
