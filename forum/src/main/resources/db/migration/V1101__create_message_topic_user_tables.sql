use `forum`;

CREATE TABLE `messages` (
 `id` INT auto_increment NOT NULL,
 `topic_id` INT NOT NULL,
 `user_id` INT NOT NULL,
 `content` varchar(255) NOT NULL,
 `login` varchar(255) NOT NULL,
 `message_date` date NOT NULL,
 `message_time` time NOT NULL,
 PRIMARY KEY (`id`)
);

create table `topics` (
 `id` INT auto_increment NOT NULL,
 `user_id` INT NOT NULL,
 `name` varchar(128) NOT NULL,
 `theme` varchar(128) NOT NULL,
 `question` varchar(128) NOT NULL,
 `login` varchar(128) NOT NULL,
 `quantity` int NOT NULL,
 `topic_date` date NOT NULL,
 `topic_time` time NOT NULL,
 PRIMARY KEY (`id`)
);

CREATE TABLE `users` (
 `id` INT auto_increment NOT NULL,
 `name` varchar(128) NOT NULL,
 `email` varchar(128) NOT NULL,
 `login` varchar(128) NOT NULL,
 `password` varchar(128) NOT NULL,
 `role` varchar(128) NOT NULL default 'USER',
 `enabled` INT NOT NULL  default true,
 PRIMARY KEY (`id`)
);
