package kg.attractor.forum.controller;
import kg.attractor.forum.repository.TopicRepository;
import kg.attractor.forum.service.MessageService;
import kg.attractor.forum.service.PropertiesService;
import kg.attractor.forum.service.TopicService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


@AllArgsConstructor
@Controller
public class MainController {
    private final TopicRepository topicRepository;
    private final TopicService topicService;
    private final MessageService messageService;
    private final PropertiesService propertiesService;


    @GetMapping
    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        var uri = uriBuilder.getRequestURI();
        var topics = topicService.getAll(pageable);
        if(!userEmail.equals("anonymousUser")){
           model.addAttribute("authorized", true);
        }
        if(topicRepository.findAll()!=null){
            constructPageable(topics, propertiesService.getDefaultPageSize(), model, uri);
        }
        return "index";
    }

    @GetMapping("/topic/{id:\\d+?}")
    public String topicPage(@PathVariable int id, Model model, Pageable pageable, HttpServletRequest uriBuilder){
        model.addAttribute("topic", topicRepository.findTopicById(id));
        var uri = uriBuilder.getRequestURI();
        var messages = messageService.getThisTopicMessages(id, pageable);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        if(!userEmail.equals("anonymousUser")){
            model.addAttribute("authorized", true);
        }

        if(messages!=null){
            constructPageable(messages, propertiesService.getDefaultPageSize(), model, uri);
        }else{
            model.addAttribute("messages", false);
        }
        return "topic-page";
    }


    @GetMapping("/new-topic")
    public String newTopic(Model model){
        return "new-topic";
    }


    @PostMapping("/new-topic")
    public String createNewTopic(@RequestParam String name, String theme, String question, Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        topicService.createNewTopic(userEmail, name, theme, question);
        return "redirect:/";
    }

    @PostMapping("/createMessage")
    public String rootSave(@RequestParam("topic_id") Integer topicId,
                           @RequestParam("message_content") String content){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userEmail = auth.getName();
        return messageService.createNewMessage(topicId, content, userEmail);
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri){
        if(list.hasNext()){
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if(list.hasPrevious()){
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }
        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }

    private static String constructPageUri(String uri, int page, int size){
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
}
