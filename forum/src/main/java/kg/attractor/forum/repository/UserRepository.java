package kg.attractor.forum.repository;

import kg.attractor.forum.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    boolean existsByEmail(String email);
    Optional<User> findByEmail(String email);
    User findUserByEmail(String email);
    User findUserById(Integer id);

}
